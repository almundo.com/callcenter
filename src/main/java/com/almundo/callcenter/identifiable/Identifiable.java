package com.almundo.callcenter.identifiable;

/**
 * Identifiable class
 */
public class Identifiable {

    private final Long id;


    public Identifiable(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identifiable identifiable = (Identifiable) o;

        return id.equals(identifiable.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
