package com.almundo.callcenter.identifiable.call;

import com.almundo.callcenter.identifiable.Identifiable;

/**
 * An identifiable Call with a specified length
 */
public class Call extends Identifiable {

    private final Integer length;

    /**
     * Call Constructor
     *
     * @param id     An id
     * @param length a length in milliseconds
     */
    Call(Long id, Integer length) {
        super(id);
        this.length = length;
    }

    public Integer getLength() {
        return length;
    }

}
