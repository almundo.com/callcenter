package com.almundo.callcenter.identifiable.call;

import com.almundo.callcenter.util.AutonumericFactory;
import com.almundo.callcenter.util.RandomRange;

/**
 * Creates identifiable calls with random running time
 */
public class CallFactory extends AutonumericFactory {
    private static CallFactory instance = null;

    public static CallFactory getInstance() {
        if (instance == null) {
            instance = new CallFactory();
        }

        return instance;
    }

    private CallFactory() {
        super();
    }

    /**
     * Creates a call with length between min and max
     *
     * @param min Minimum random call length
     * @param max Maximum random call length
     * @return An identifiable Call of random length between min and max
     */
    public Call createCall(Integer min, Integer max) {
        if(min<0){
            throw new IllegalArgumentException("Min value must be positive.");
        }

        if(max<0){
            throw new IllegalArgumentException("Max value must be positive.");
        }

        return new Call(getAndIncrementId(), RandomRange.getRandomNumberInRange(min, max));
    }

}
