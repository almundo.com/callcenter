package com.almundo.callcenter.identifiable.recipient;


import com.almundo.callcenter.identifiable.Identifiable;
import com.almundo.callcenter.identifiable.call.Call;

import java.util.concurrent.TimeUnit;

/**
 * An inmutable recipient able to answer calls
 */
public class Recipient extends Identifiable {
    private final RecipientType type;

    /**
     * Constructor for a given id and type
     *
     * @param id   An identifier
     * @param type A RecipientType
     */
    Recipient(Long id, RecipientType type) {
        super(id);
        this.type = type;
    }

    public RecipientType getType() {
        return type;
    }

    /**
     * Emulates answering a call, keeping the recipient busy
     *
     * @param call A call to be answered
     * @throws InterruptedException If interrupted prior to reach the end of the call length
     */
    public void answer(Call call) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(call.getLength());
    }

}
