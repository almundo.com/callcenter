package com.almundo.callcenter.identifiable.recipient;

/**
 * Specifies recipient types
 */
public enum RecipientType {

    OPERATOR,
    SUPERVISOR,
    DIRECTOR,
    CALL_RECORDER
}
