package com.almundo.callcenter.identifiable.recipient;

import com.almundo.callcenter.util.AutonumericFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Singleton factory of typed identifiable recipients
 */
public class RecipientFactory extends AutonumericFactory {
    private static RecipientFactory instance = null;

    public static RecipientFactory getInstance() {
        if (instance == null) {
            instance = new RecipientFactory();
        }

        return instance;
    }

    private RecipientFactory() {
        super();
    }

    public Recipient createRecipient(RecipientType type) {
        return new Recipient(this.getAndIncrementId(), type);
    }

    /**
     * @param recipientTypeQty A Map containing a RecipientType as key and an Integer specifying
     *                         the number of desired identifiable instances
     * @return A collection of new recipients as specified on recipientTypeQty
     */
    public Collection<Recipient> createRecipients(Map<RecipientType, Integer> recipientTypeQty) {
        Collection<Recipient> recipients = new ArrayList<>();
        recipientTypeQty.forEach((key, value) -> {
            for (Integer i = 0; i < value; i++) {
                recipients.add(createRecipient(key));
            }
        });

        return recipients;
    }

}