package com.almundo.callcenter;

/**
 * Convenient application configurable constants
 */
public interface ConfigConstants {

    Long PRODUCER_RUN_MILLIS = 30000L;

    Integer CALL_PRODUCER_MIN_THREAD_COUNT = 10;
    Integer CALL_DISPATCHER_THREAD_COUNT = 10;
    Integer CALL_LENGTH_MIN = 5000;
    Integer CALL_LENGTH_MAX = 10000;
    Long CALL_PERIOD = 500L;
    Long CALL_INITIAL_DELAY = 0L;
    Long WORKER_AWAIT_TERMINATION = 300000L;
    Boolean CALL_RECORDER_RECIPIENT_ENABLED = Boolean.FALSE;

    Integer OPERATOR_QTY = 5;
    Integer SUPERVISOR_QTY = 2;
    Integer DIRECTOR_QTY = 1;

}