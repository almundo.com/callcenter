package com.almundo.callcenter;

import com.almundo.callcenter.identifiable.call.CallFactory;
import com.almundo.callcenter.identifiable.recipient.Recipient;
import com.almundo.callcenter.identifiable.recipient.RecipientFactory;
import com.almundo.callcenter.identifiable.recipient.RecipientType;
import com.almundo.callcenter.worker.CallProducer;
import com.almundo.callcenter.worker.Dispatcher;
import com.almundo.callcenter.worker.impl.CallProducerImpl;
import com.almundo.callcenter.worker.impl.CallRecorderEnabledRecipientPriorityDispatcherImpl;
import com.almundo.callcenter.worker.impl.RecipientPriorityDispatcherImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.almundo.callcenter.ConfigConstants.*;

/**
 * Call Center Application main class
 */
public class App {
    private final static Logger logger = LogManager.getLogger(App.class);

    private CallProducer producer;
    private Dispatcher dispatcher;

    private CallProducer getProducer() {
        return producer;
    }

    private Dispatcher getDispatcher() {
        return dispatcher;
    }

    public static void main(String[] args) {

        App app = new App();

        app.init();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (app.getProducer().isRunning()) {
                logger.info("ShutdownStoppingHook reached with producers already running. Gracefully stopping producers.");
                app.getProducer().stop();
            }

            if (app.getDispatcher().isRunning()) {
                logger.info("ShutdownStoppingHook reached with consumers already running. Gracefully stopping consumers.");
                app.getDispatcher().stop();
            }
        }));
        app.run();
    }

    private void init() {
        logger.info("Initializing...");

        Map<RecipientType, Integer> recipientTypeQty = new HashMap<>();
        recipientTypeQty.put(RecipientType.OPERATOR, OPERATOR_QTY);
        recipientTypeQty.put(RecipientType.SUPERVISOR, SUPERVISOR_QTY);
        recipientTypeQty.put(RecipientType.DIRECTOR, DIRECTOR_QTY);

        Collection<Recipient> recipients = RecipientFactory.getInstance().createRecipients(recipientTypeQty);
        logger.debug("Recipients initialized");

        if (CALL_RECORDER_RECIPIENT_ENABLED) {
            this.dispatcher = new CallRecorderEnabledRecipientPriorityDispatcherImpl(CALL_DISPATCHER_THREAD_COUNT, recipients);
        } else {
            this.dispatcher = new RecipientPriorityDispatcherImpl(CALL_DISPATCHER_THREAD_COUNT, recipients);
        }
        logger.debug("Dispatcher initialized");
        logger.info("Initialized.");

        producer = new CallProducerImpl(CallFactory.getInstance(), CALL_LENGTH_MIN, CALL_LENGTH_MAX, CALL_PRODUCER_MIN_THREAD_COUNT, dispatcher, CALL_INITIAL_DELAY, CALL_PERIOD);
        logger.debug("Call Producer initialized.");
    }

    private void run() {

        producer.start();
        try {
            TimeUnit.MILLISECONDS.sleep(PRODUCER_RUN_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        producer.stop();
        dispatcher.stop();
    }
}