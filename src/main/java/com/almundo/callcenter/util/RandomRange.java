package com.almundo.callcenter.util;

import java.util.Random;

 /**
 * Utility for retrieving random numbers in a specified range
 */
public class RandomRange {
    /**
     * Retrieves random integers within specified min and max values
     *
     * @param min Minimum random number
     * @param max Maximum random number
     * @return A random Integer between min and max
     */
    public static Integer getRandomNumberInRange(Integer min, Integer max) {
        Integer number;
        if (min > max) {
            throw new IllegalArgumentException("max must be greater than min");
        } else if (min.equals(max)) number = min;
        else {
            Random r = new Random();
            number = r.nextInt((max - min) + 1) + min;
        }
        return number;
    }
}