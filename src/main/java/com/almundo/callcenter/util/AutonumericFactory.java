package com.almundo.callcenter.util;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A factory that provides consecutive identifiers
 */
public class AutonumericFactory {
    private final AtomicLong id = new AtomicLong(0);

    /**
     * Generates consecutive long numbers
     *
     * @return A consecutive long value
     */
    protected Long getAndIncrementId() {
        return id.getAndIncrement();
    }

}
