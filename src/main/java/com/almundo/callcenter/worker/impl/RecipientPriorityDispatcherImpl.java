package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.identifiable.call.Call;
import com.almundo.callcenter.identifiable.recipient.Recipient;
import com.almundo.callcenter.worker.Dispatcher;

import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Dispatch calls to available recipients
 */
public class RecipientPriorityDispatcherImpl extends GenericWorkerImpl implements Dispatcher {

    final PriorityBlockingQueue<Recipient> recipients;


    /**
     * RecipientPriorityDispatcherImpl constructor
     *
     * @param concurrentCalls Amount of supported concurrent calls
     * @param recipients      A collection of available recipient for answering calls
     */
    public RecipientPriorityDispatcherImpl(Integer concurrentCalls, Collection<Recipient> recipients) {
        super(Executors.newFixedThreadPool(concurrentCalls));
        this.recipients = new PriorityBlockingQueue<>(recipients.size(), Comparator.comparing(Recipient::getType));
        this.recipients.addAll(recipients);
    }

    /**
     * Dispatch a call to a prioritized list of recipients.
     * In case all recipients are busy, it blocks thread execution until
     * any recipient is available.
     *
     * @param call an inbound call to be dispatched
     */
    public void dispatchCall(Call call) {
        logger.info("[call id: " + call.getId() + "] RECEIVED");
        this.executorService.submit(() -> {
            Recipient recipient = null;
            try {
                logger.info("[call id: " + call.getId() + "] Avaliable recipients " + recipients);
                recipient = getRecipient();
                logger.info("[call id: " + call.getId() + "] DISPATCHED to recipient type " + recipient.getType().name() + ". id: " + recipient.getId());
                recipient.answer(call);
                logger.info("[call id: " + call.getId() + "] FINISHED, recipient type " + recipient.getType().name() + ". id: " + recipient.getId());
            } catch (InterruptedException e) {
                logger.info("[call id: " + call.getId() + "] INTERRUPTED, Could NOT retrieve recipient for call");
            } finally {
                if (recipient != null) {
                    recipients.put(recipient);
                    logger.info("[call id: " + call.getId() + "] RELEASED recipient type " + recipient.getType().name() + ". id: " + recipient.getId());
                }
            }
        });
    }

    /**
     * Obtains the highest priority available recipient or blocks until one is released
     *
     * @return An available recipient
     */
    protected Recipient getRecipient() throws InterruptedException {
        return recipients.take();
    }

}
