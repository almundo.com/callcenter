package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.identifiable.recipient.Recipient;
import com.almundo.callcenter.identifiable.recipient.RecipientFactory;
import com.almundo.callcenter.identifiable.recipient.RecipientType;
import com.almundo.callcenter.worker.Dispatcher;

import java.util.Collection;

/**
 * Dispatch calls to available recipients
 */
public class CallRecorderEnabledRecipientPriorityDispatcherImpl extends RecipientPriorityDispatcherImpl implements Dispatcher {

    /**
     * RecipientPriorityDispatcherImpl constructor
     *
     * @param concurrentCalls Amount of supported concurrent calls
     * @param recipients      A collection of available recipient for answering calls
     */
    public CallRecorderEnabledRecipientPriorityDispatcherImpl(Integer concurrentCalls, Collection<Recipient> recipients) {
        super(concurrentCalls, recipients);
    }

    /**
     * Obtains the highest priority recipient available or an automatized new one
     *
     * @return An available recipient or a new CALL_RECORDER type recipient
     */
    @Override
    protected Recipient getRecipient() {
        Recipient recipient;
        if ((recipient = recipients.poll()) == null) {
            recipient = RecipientFactory.getInstance().createRecipient(RecipientType.CALL_RECORDER);
        }
        return recipient;
    }
}
