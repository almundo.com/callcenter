package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.identifiable.call.Call;
import com.almundo.callcenter.identifiable.call.CallFactory;
import com.almundo.callcenter.worker.CallProducer;
import com.almundo.callcenter.worker.Dispatcher;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Produces calls for specified duration range
 */
public class CallProducerImpl extends GenericWorkerImpl implements CallProducer {

    private final Dispatcher dispatcher;

    private final CallFactory callFactory;
    private final Integer minCallLength;
    private final Integer maxCallLength;
    private final Long initialDelay;
    private final Long rate;

    /**
     * CallProducerImpl constructor
     *
     * @param callFactory     Factory for creating calls
     * @param minCallLength   Min call running time
     * @param maxCallLength   Max call running time
     * @param concurrentCalls Number of active calls at the same time
     * @param dispatcher      Consumer to dispatch calls
     * @param initialDelay    Delay prior to creation rate
     * @param rate            Creation rate
     */
    public CallProducerImpl(CallFactory callFactory, Integer minCallLength, Integer maxCallLength, Integer concurrentCalls, Dispatcher dispatcher, Long initialDelay, Long rate) {
        super(Executors.newScheduledThreadPool(concurrentCalls));
        this.dispatcher = dispatcher;
        this.callFactory = callFactory;
        this.initialDelay = initialDelay;
        this.minCallLength = minCallLength;
        this.maxCallLength = maxCallLength;
        this.rate = rate;
    }

    /**
     * Starts producing calls according to parameters specified on constructor
     */
    @Override
    public void start() {
        ((ScheduledExecutorService) this.executorService).scheduleAtFixedRate(
                () -> {
                    Call call = callFactory.createCall(minCallLength, maxCallLength);

                    logger.info("[call id: " + call.getId() + "] DIALING");
                    this.dispatcher.dispatchCall(call);
                    logger.info("[call id: " + call.getId() + "] TRANSFERRED");
                },
                initialDelay,
                rate,
                TimeUnit.MILLISECONDS
        );
    }
}
