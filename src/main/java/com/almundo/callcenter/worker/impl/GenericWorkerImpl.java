package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.ConfigConstants;
import com.almundo.callcenter.worker.Worker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Implements generic worker methods
 */
public class GenericWorkerImpl implements Worker {

    protected Logger logger = LogManager.getLogger(getClass());

    protected final ExecutorService executorService;

    public GenericWorkerImpl(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public Boolean isRunning() {
        return !this.executorService.isTerminated();
    }

    @Override
    public void stop() {
        logger.info("STOP INVOKED. Not accepting further tasks");
        this.executorService.shutdown();
        Long shutdownTime = System.currentTimeMillis();
        try {
            this.executorService.awaitTermination(ConfigConstants.WORKER_AWAIT_TERMINATION, TimeUnit.MILLISECONDS);
            logger.info("All ongoing calls finished after " + (System.currentTimeMillis() - shutdownTime) + " ms of being requested.");
        } catch (InterruptedException e) {
            logger.info("Interrupted while awaiting ongoing calls to finish after " + (System.currentTimeMillis() - shutdownTime));
        }
    }

}
