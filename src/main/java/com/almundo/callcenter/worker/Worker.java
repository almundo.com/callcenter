package com.almundo.callcenter.worker;

/**
 * Worker abstraction for stopping and querying its status
 */
public interface Worker {

    /**
     * Tell if the Worker is running (or not)
     *
     * @return True if the Worker is running, False if it is not.
     */
    Boolean isRunning();

    /**
     * Stops the worker
     */
    void stop();

}