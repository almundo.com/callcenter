package com.almundo.callcenter.worker;

/**
 * Consumer interface for Dispatcher segregation
 */
public interface CallProducer extends Worker {

    /**
     * Starts the producer
     */
    void start();

}
