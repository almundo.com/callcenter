package com.almundo.callcenter.worker;

import com.almundo.callcenter.identifiable.call.Call;

/**
 * Consumer interface for Dispatcher segregation
 */
public interface Dispatcher extends Worker {
    /**
     * Dispatch a call
     *
     * @param call a call
     * @return Recipient that answered the call
     */
    void dispatchCall(Call call);

}
