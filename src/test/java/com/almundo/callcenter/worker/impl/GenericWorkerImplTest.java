package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.identifiable.call.Call;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by hernan on 16/07/17.
 */
public class GenericWorkerImplTest {

    GenericWorkerImpl worker;

    @Before
    public void setUp() {
        worker = new GenericWorkerImpl(new MockExecutor<Call>());
    }


    @Test
    public void workerReflectsIfExecutorIsRunning() throws Exception {
        assertTrue(worker.isRunning());

    }

    @Test
    public void workerStopsExecutor() throws Exception {
        assertTrue(worker.isRunning());
        worker.stop();
        assertFalse(worker.isRunning());
    }

    @Test
    public void awaitTerminationInterruptedExceptionMustBeCatched() throws Exception {
        worker=new GenericWorkerImpl(new MockExecutor<Call>(Boolean.TRUE));
        assertTrue(worker.isRunning());
        worker.stop();
        assertFalse(worker.isRunning());
    }

    class MockExecutor<T> implements ExecutorService {
        private Boolean isRunning, isTerminated, throwInterruptedException;

        public MockExecutor() {
            this.isRunning = Boolean.TRUE;
            this.isTerminated = Boolean.FALSE;
            this.throwInterruptedException = Boolean.FALSE;
        }

        public MockExecutor(Boolean throwInterruptedException) {
            this();
            this.throwInterruptedException= throwInterruptedException;
        }


        @Override
        public void shutdown() {
            this.isRunning = Boolean.FALSE;
        }

        @Override
        public List<Runnable> shutdownNow() {
            return null;
        }

        @Override
        public boolean isShutdown() {
            return !isRunning;
        }

        @Override
        public boolean isTerminated() {
            return !isRunning;
        }

        @Override
        public boolean awaitTermination(long l, TimeUnit timeUnit) throws InterruptedException {
            if(throwInterruptedException){
                throw new InterruptedException();
            }
            return false;
        }

        @Override
        public <T> Future<T> submit(Callable<T> callable) {
            return null;
        }

        @Override
        public <T> Future<T> submit(Runnable runnable, T t) {
            return null;
        }

        @Override
        public Future<?> submit(Runnable runnable) {
            return null;
        }

        @Override
        public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
            return null;
        }

        @Override
        public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit) throws InterruptedException {
            return null;
        }

        @Override
        public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
            return null;
        }

        @Override
        public <T> T invokeAny(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
            return null;
        }

        @Override
        public void execute(Runnable runnable) {

        }
    }
}