package com.almundo.callcenter.worker.impl;

import com.almundo.callcenter.identifiable.call.Call;
import com.almundo.callcenter.identifiable.call.CallFactory;
import com.almundo.callcenter.identifiable.recipient.Recipient;
import com.almundo.callcenter.identifiable.recipient.RecipientFactory;
import com.almundo.callcenter.identifiable.recipient.RecipientType;
import com.almundo.callcenter.worker.Dispatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * CallRecorderEnabledRecipientPriorityDispatcherImpl Test
 */
public class CallRecorderEnabledRecipientPriorityDispatcherImplTest {
    private CallFactory callFactory;
    private DispatcherState state;
    private Dispatcher dispatcher;

    private class DispatcherState {
        Integer maxCallLength;
        List<Call> calls = new ArrayList<>();
        Collection<Recipient> recipients = new ArrayList<>();
    }

    @Before
    public void setUp() {
        callFactory = CallFactory.getInstance();
        Map<RecipientType, Integer> recipientTypeQty = new HashMap<>();
        recipientTypeQty.put(RecipientType.OPERATOR, 1);
        recipientTypeQty.put(RecipientType.SUPERVISOR, 1);
        recipientTypeQty.put(RecipientType.DIRECTOR, 1);

        state = init(10, 5000, 10000, recipientTypeQty);
        dispatcher = new CallRecorderEnabledRecipientPriorityDispatcherImpl(10, state.recipients);
    }

    private DispatcherState init(final Integer callsQty, final Integer minCallLength, final Integer maxCallLength, final Map<RecipientType, Integer> recipientTypeQty) {
        DispatcherState state = new DispatcherState();
        state.maxCallLength = maxCallLength;
        for (int i = 0; i < callsQty; i++) {
            state.calls.add(callFactory.createCall(minCallLength, maxCallLength));
        }

        state.recipients = RecipientFactory.getInstance().createRecipients(recipientTypeQty);

        return state;
    }

    /**
     * Dispatches ten concurrent calls
     */
    @Test
    public void dispatchTenConcurrentCalls() {

        state.calls.parallelStream().forEach(dispatcher::dispatchCall);
        dispatcher.stop();
    }

}
