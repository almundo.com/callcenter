package com.almundo.callcenter.identifiable;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * IdentifiableTest
 */
public class IdentifiableTest {
    private Identifiable a = null;
    private Identifiable b = null;

    @Before
    public void setUp() throws Exception {
        a = new Identifiable(23L);
        b = new Identifiable(23L);
    }

    @After
    public void tearDown() throws Exception {
        a = null;
        b = null;
    }

    @Test
    public void testGetId() throws Exception {
        Assert.assertEquals(23L, a.getId().longValue());
    }

    @Test
    public void testEquals() throws Exception {
        Assert.assertEquals(a, b);
    }

    @Test
    public void testHashCode() throws Exception {
        Assert.assertEquals(a.hashCode(), b.hashCode());
    }
}