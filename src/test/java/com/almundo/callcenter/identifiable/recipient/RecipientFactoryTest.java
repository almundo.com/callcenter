package com.almundo.callcenter.identifiable.recipient;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * RecipientFactoryTest
 */
public class RecipientFactoryTest {
    private RecipientFactory recipientFactory;
    private Recipient recipient;

    @Before
    public void setUp() throws Exception {
        recipientFactory = null;
        recipient = null;

    }

    @After
    public void tearDown() throws Exception {
        recipientFactory = null;
        recipient = null;
    }

    @Test
    public void createAnsweringMachineRecipient() {
        givenARecipientFactory();
        whenCreateRecipientOfType(RecipientType.CALL_RECORDER);
        thenRecipientTypeIs(RecipientType.CALL_RECORDER);
    }

    @Test
    public void createDirectorRecipient() {
        givenARecipientFactory();
        whenCreateRecipientOfType(RecipientType.DIRECTOR);
        thenRecipientTypeIs(RecipientType.DIRECTOR);
    }

    @Test
    public void createSupervisorRecipient() {
        givenARecipientFactory();
        whenCreateRecipientOfType(RecipientType.SUPERVISOR);
        thenRecipientTypeIs(RecipientType.SUPERVISOR);
    }

    @Test
    public void createOperatorRecipient() {
        givenARecipientFactory();
        whenCreateRecipientOfType(RecipientType.OPERATOR);
        thenRecipientTypeIs(RecipientType.OPERATOR);
    }

    private void givenARecipientFactory() {
        recipientFactory = RecipientFactory.getInstance();
    }

    private void whenCreateRecipientOfType(RecipientType recipientType) {
        recipient = recipientFactory.createRecipient(recipientType);
    }

    private void thenRecipientTypeIs(RecipientType recipientType) {
        Assert.assertEquals(recipient.getType(), recipientType);
    }


    @Test
    public void bulkRecipientCreation() {
        int operatorQty = 4;
        int supervisorQty = 2;
        int directorQty = 1;
        int answeringMachineQty = 10;

        givenARecipientFactory();

        Map<RecipientType, Integer> recipientTypeQty = new HashMap<>();
        recipientTypeQty.put(RecipientType.OPERATOR, operatorQty);
        recipientTypeQty.put(RecipientType.SUPERVISOR, supervisorQty);
        recipientTypeQty.put(RecipientType.DIRECTOR, directorQty);
        recipientTypeQty.put(RecipientType.CALL_RECORDER, answeringMachineQty);
        Collection<Recipient> recipients = recipientFactory.createRecipients(recipientTypeQty);
        
        Map<RecipientType, List<Recipient>> recipientsByType = recipients.stream().collect(Collectors.groupingBy(Recipient::getType));
        Assert.assertEquals(operatorQty, recipientsByType.getOrDefault(RecipientType.OPERATOR, new ArrayList<>()).size());
        Assert.assertEquals(supervisorQty, recipientsByType.getOrDefault(RecipientType.SUPERVISOR, new ArrayList<>()).size());
        Assert.assertEquals(directorQty, recipientsByType.getOrDefault(RecipientType.DIRECTOR, new ArrayList<>()).size());
        Assert.assertEquals(answeringMachineQty, recipientsByType.getOrDefault(RecipientType.CALL_RECORDER, new ArrayList<>()).size());


    }

}