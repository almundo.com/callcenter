package com.almundo.callcenter.identifiable.recipient;

import com.almundo.callcenter.identifiable.call.Call;
import com.almundo.callcenter.identifiable.call.CallFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by hernan on 16/07/17.
 */
public class RecipientTest {

    private Recipient recipient;

    @Before
    public void setUp() throws Exception {
        recipient=new Recipient(0L, RecipientType.OPERATOR);

    }

    @After
    public void tearDown() throws Exception {
        recipient = null;
    }

    @Test
    public void answer() throws Exception {
        Call call = CallFactory.getInstance().createCall(1, 1);
        recipient.answer(call);
    }

}