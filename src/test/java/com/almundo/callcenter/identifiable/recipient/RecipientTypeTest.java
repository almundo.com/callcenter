package com.almundo.callcenter.identifiable.recipient;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * RecipientTypeTest
 */
public class RecipientTypeTest {

    private RecipientType t0, t1, t2, t3;

    @Before
    public void setUp() throws Exception {
        t0 = RecipientType.OPERATOR;
        t1 = RecipientType.SUPERVISOR;
        t2 = RecipientType.DIRECTOR;
        t3 = RecipientType.CALL_RECORDER;
    }

    @After
    public void tearDown() throws Exception {
        t0=null;
        t1=null;
        t2=null;
        t3=null;

    }

    @Test
    public void name() throws Exception {
    }

    @Test
    public void supervisorGtOperator() throws Exception {
        Assert.assertTrue(RecipientType.SUPERVISOR.compareTo(RecipientType.OPERATOR)>0);
    }

    @Test
    public void directorGtSupervisor() throws Exception {
        Assert.assertTrue(RecipientType.DIRECTOR.compareTo(RecipientType.SUPERVISOR)>0);
    }

    @Test
    public void answeringMachineGtDirector() throws Exception {
        Assert.assertTrue(RecipientType.CALL_RECORDER.compareTo(RecipientType.DIRECTOR)>0);
    }



    @Test
    public void compareTo() throws Exception {
    }

    @Test
    public void valueOf() throws Exception {
    }

}