package com.almundo.callcenter.identifiable.call;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * CallFactoryTest
 */
public class CallFactoryTest {
    private CallFactory callFactory = null;
    private Call call = null;

    @Before
    public void setUp() throws Exception {
        callFactory = CallFactory.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        callFactory = null;
        call = null;
    }

    @Test
    public void getInstance() throws Exception {
        Assert.assertTrue(callFactory != null && callFactory.getClass().isAssignableFrom(CallFactory.class));
    }

    @Test
    public void whenEqualsMinAndMaxLengthMustBeThatValue() throws Exception {
        Integer anyValue = 6;
        whenMinAndMaxLengthAreTheSame(anyValue);
        thenCallLengthMustBe(anyValue);
    }

    private void whenMinAndMaxLengthAreTheSame(Integer anyValue) {
        givenACallWhenMinAndMax(anyValue, anyValue);
    }

    private void thenCallLengthMustBe(Integer anyValue) {
        Assert.assertTrue(call != null && call.getLength().equals(anyValue));
    }

    @Test
    public void testOneHundredCallsCreatedWithinSpecifiedLengthRange() {
        IntStream.range(0, 100).forEach(i -> {
            Integer minValue = i;
            Integer maxValue = i + 3;
            givenACallWhenMinAndMax(minValue, maxValue);
            thenCallLengthMustBeBetween(minValue, maxValue);
        });
    }

    private void thenCallLengthMustBeBetween(Integer minValue, Integer maxValue) {
        assertTrue(call.getLength() >= minValue && call.getLength() <= maxValue);
    }

    private void givenACallWhenMinAndMax(int min, int max) {
        call = callFactory.createCall(min, max);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeCallMinLengthMustThrowIllegalArgumentException() {
        givenACallWhenMinAndMax(-1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeCallMaxLengthMustThrowIllegalArgumentException() {
        givenACallWhenMinAndMax(1, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeCallMinAndMaxLengthMustThrowIllegalArgumentException() {

        givenACallWhenMinAndMax(-1, -1);
    }
}