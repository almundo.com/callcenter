package com.almundo.callcenter.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.LongStream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for RandomRange
 */
public class AutonumericFactoryTest {

    @Test
    public void ensureSequentialNumbers() {
        AutonumericFactory f = new AutonumericFactory();
        LongStream.range(0, 100).forEach(i -> assertEquals(i, f.getAndIncrementId().longValue()));
    }

    @Test
    public void ensureSequentialThreadSafeNumbers() {
        Set<Long> sequence = new ConcurrentSkipListSet<>();
        AutonumericFactory f = new AutonumericFactory();
        LongStream.range(0, 100).parallel().forEach(i -> sequence.add(f.getAndIncrementId()));

        Iterator<Long> seqIt = sequence.iterator();
        LongStream.range(0, 100).forEach(i -> Assert.assertEquals(i, seqIt.next().longValue()));
    }
}
