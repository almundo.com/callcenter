package com.almundo.callcenter.util;

import org.junit.Test;

import java.util.stream.IntStream;

import static com.almundo.callcenter.util.RandomRange.getRandomNumberInRange;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for RandomRange
 */
public class RandomRangeTest {

    @Test
    public void whenMinEqualsMaxMustReturnThatValue() {
        assertEquals(new Integer(6), getRandomNumberInRange(6, 6));
    }


    @Test
    public void ensureValueWithinRangeRegardlessPositiveness() {
        IntStream.range(-100, 100).forEach(i -> {
            Integer value = getRandomNumberInRange(i, i + 3);
            assertTrue(value >= i && value <= i + 3);
        });
    }


    @Test(expected = IllegalArgumentException.class)
    public void whenMinValueGtMaxValue() {
        getRandomNumberInRange(4,3);
    }

}
