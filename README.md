# Call Center

## Consigna

Existe un call center donde hay 3 tipos de empleados: operador,
supervisor y director. El proceso de la atención de una llamada
telefónica en primera instancia debe ser atendida por un operador, si
no hay ninguno libre debe ser atendida por un supervisor, y de no
haber tampoco supervisores libres debe ser atendida por un director.


## Requerimientos

- Debe existir una clase Dispatcher encargada de manejar lasllamadas, y debe contener el método dispatchCall para que las
asigne a los empleados disponibles.
- La clase Dispatcher debe tener la capacidad de poder procesar
10 llamadas al mismo tiempo (de modo concurrente).
- Cada llamada puede durar un tiempo aleatorio entre 5 y 10
segundos.
- Debe tener un test unitario donde lleguen 10 llamadas.


## Extras/Plus

- Dar alguna solución sobre qué pasa con una llamada cuando no
hay ningún empleado libre.
- Dar alguna solución sobre qué pasa con una llamada cuando
entran más de 10 llamadas concurrentes.
- Agregar los tests unitarios que se crean convenientes.
- Agregar documentación de código.


## Tener en Cuenta

- El proyecto debe ser creado con Maven.
- De ser necesario, anexar un documento con la explicación del
cómo y porqué resolvió los puntos extras, o comentarlo en las
clases donde se encuentran sus tests unitarios respectivos.


# Notas sobre la resolución

- Se provee una interfaz ConfigConstants conteniendo algunas constantes para permitir alterar parametría del comportamiento de la solución.
- Se provee un productor de llamadas en la clase CallProducerImpl para simular la generacion de llamadas.
- Se provee una clase RandomRange encargada de devolver un número aleatorio a partir de un rango proporcionado para proveer la longitud de las llamadas generadas por el simulador de llamadas provisto.
- A fin de que el Dispatcher soporte 10 llamadas concurrentes se hace uso de un pool fijo de 10 threads. Este valor es configurable desde las constantes de configuración.
- Cuando entran más de 10 llamados concurrentes se encolan en la LinkedBlockingQueue del ThreadPoolExecutor que utiliza el dispatcher.
- Para cuando no hay ningún empleado libre se provee como solución un contestador automático que permite grabar la llamada (Siempre respetando el requisito de 10 llamadas concurrentes). Para ello se proveen dos implementaciones del Dispatcher: una con dicha solución y otra en la que sólo atienden los empleados disponibles.
- En la implementación provista Dispatcher es una interfaz y no una clase. Esto permite proveer dos implementaciones para cubrir los Extras/Plus.
